---
author: thoughtpolice
title: "GHC Weekly News - 2014/12/08"
date: 2014-12-08T15:33:33
tags: ghc news
---

Hi \*,

Once more, it's time for some news about GHC! This week's regularly scheduled programming (get it?) has brought you...

  - As of last week, GHC officially has no more `.lhs` files in its source repository; instead, all files have been converted to `.hs` and are now much more consistent with each other: https://www.haskell.org/pipermail/ghc-devs/2014-December/007552.html

  - Joachim Breitner has reported that the `linker_unload` test in GHC has been failing, but it's been surprisingly hard to reproduce reliably on our build machines! https://www.haskell.org/pipermail/ghc-devs/2014-December/007528.html

  - Moritz Angermann posted a proposal about the "Out of Process Template Haskell" project, started by the GHCJS developers. In short, they want to work out how to get Template Haskell working in a stage2 GHC for things like iOS or Browser devices: https://www.haskell.org/pipermail/ghc-devs/2014-December/007555.html

  - Lennart Augustsson has an inquiry about his program: why is it running out of memory? But the stranger thing: why does it only run out if heap profiling *is not enabled*? Nobody has quite figured out, but if you're a guru, it may be a good chance to help out: https://www.haskell.org/pipermail/ghc-devs/2014-December/007582.html

  - Yuras Shumovich tracked down some nasty bugs in the typechecker's linter, causing several programs to fail to work when compiled by GHC. A quick diagnosis, but no fix has been merged quite yet: https://www.haskell.org/pipermail/ghc-devs/2014-December/007580.html

  - Richard Eisenberg wants feedback on a what he thinks is a design wart in the use of `-XStandaloneDeriving`, and he's not only proposed a solution, but wants to know what people think; typechecking fans are surely puzzling away already: https://www.haskell.org/pipermail/ghc-devs/2014-December/007589.html

  - David Spies has run into an interesting situation: why does -O make his program **slower** instead of faster? Well, nobody has quite figured out why yet, but it's an interesting question - maybe on a lazy monday developer can help figure out: https://www.haskell.org/pipermail/ghc-devs/2014-December/thread.html

  - Richard E. has another thread on the list, this time about development work flows: what do we do about painful merges? https://www.haskell.org/pipermail/ghc-devs/2014-December/007586.html

Closed tickets this week include: #9850, #9005, #9828, #9833, #9582, #8935, #9186, #9480, #9497, #7908, #4347, #3977, #3859, #3844, #3814, #3771, #3739, #2182, #9812, #4921, #7947, #9240, #5401, #3625, #3517, #9444, #9142, #3447, #8894, #3065, #3191, #2697, #2836, #5443, #7736, #2489, #2456, #2204, #9777, #9859, #9869, #9808
