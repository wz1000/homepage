---
author: thoughtpolice
title: "GHC Weekly News - 2015/06/10"
date: 2015-06-10T21:34:37
tags: ghc news
---

Hi \*,

Welcome for the latest entry in the GHC Weekly News. The past week, GHC HQ met up for a quick catch up on 7.10.2 (which you'll want to read up on, see below), and some other technical nonsense about some work we've been doing. As a result the current weekly notes have been slow - the current priority is the next release though, which leads us to...

## 7.10.2 status

7.10.2 is **going to be out soon** - our current plan is to have a release candidate on **the weekend of Saturday the 13th**, and the final release **the next week**. That means if you want something fixed, you'd better hollar *very* soon, or we're just not going to get to it!

If you're wondering what's currently been fixed/scheduled,  the [status page](https://ghc.haskell.org/trac/ghc/wiki/Status/GHC-7.10.2) shows the current set of tickets we've fixed and plan on fixing.

## List chatter

  - Edward Z. Yang has written up a new wiki page to clearly explain and document all the various confusion around package keys, package ids, etc as a result of all the new Backpack work. If you're interested in this, it's definitely worth a read. https://mail.haskell.org/pipermail/ghc-devs/2015-June/009173.html
  
  - Mark Lentczner sez: The Haskell Platform has finally outgrown Travis-CI, now going beyond the 50 minute build limit. Mark asks what alternatives we can use going forward. https://mail.haskell.org/pipermail/ghc-devs/2015-June/009174.html

  - Jan Stolarek asks: in some cases, GHC will generate default instances or values, but that source code has no textual information location (for example, consider an `instance` clause without the `where`) - what do people think about fixing this, and are there anythings to look out for? https://mail.haskell.org/pipermail/ghc-devs/2015-June/009202.html

  - David Luposchainsky has opened a new thread - about moving `fail` out of `Monad` and into its own typeclass, `MonadFail`. This change is a request that's very long in the tooth (older than the AMP or FTP changes by a landslide), but David's proposal has a clearly set out goal to tackle compatibility, warnings, and implementation. https://mail.haskell.org/pipermail/ghc-devs/2015-June/009186.html

## Noteworthy commits

  - Commit 19ec6a84d6344c2808d0d41da11956689a0e4ae9 - Fix for CAF retention when dynamically loading & unloading code

  - Commit 4a0b7a10442eec3747d5f95ef186a79bb0648754 - Build: run autoreconf jobs in parallel

## Closed tickets

#10460, #7672, #9252, #9506, #10294, #5316, #10408, #10386, #9960, #10145, #9259, #10386, #9507, #8723, #10442, #5014, #4215, #10443, #8244, #10499, #10500, #10428, #10488, #10489, #10406, #10501, #10441, #10406, #10502, #9101, #9663, and #9945.
